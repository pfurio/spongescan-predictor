//
//  transcript.h
//  predictor
//
//  Created by Pedro Furió Tarí on 04/07/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#ifndef __predictor__transcript__
#define __predictor__transcript__

#include "exon.h"

#include <iostream>
#include <string>
#include <cmath>
#include <unordered_map>
#include <vector>
#include <algorithm>    // std::sort
#include <queue>

#endif /* defined(__predictor__transcript__) */

using namespace std;

class Gene;

class Transcript {
    
    string trans_id;
    string trans_name;
    string biotype;
    int start;
    int end;
    Gene *gene;  // Pointer to gene in which we find the transcript
    
    vector<Exon*> exons;
    
    unordered_map<string, vector<int>> positions;
    unordered_map<string, int> inner_distance; // inner_distance has a 1 for kmers with a distance between binding sites > 2*|kmer| and 0 for the rest. We will only take into account those pairs kmer-sequence with a distance between its binding sites > 2*|kmer|
    
public:
    // Constructor + setters
    Transcript(string trans_id, string trans_name, string biotype, int start, int end, Gene *gene);
    void addExon(int start, int end, int number);
    
    int addPosition(string kmer, int position); // Returns false when kmer was not in the hash, otherwise true
    void setInnerDistance();
    
    // Getters
    int getLength();               // Returns the sequence length
    string getTransID();
    string getTransName();
    string getBiotype();
    int getStart();
    int getEnd();
    vector<Exon*> getExons();
    int getInnerDistance(string kmer);
    
    // Additional methods
    int getTotalOccurrences(string kmer);
    int getMaxOcurrences(int sliding_window, string kmer);
    int getMedianDistance(string kmer);
    vector<string> getAllKmers();
    vector<int> getPositions(string kmer);
    Gene * getGene();
};
