//
//  gene.h
//  predictor
//
//  Created by Pedro Furió Tarí on 04/07/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#ifndef __predictor__gene__
#define __predictor__gene__

#include "transcript.h"

#include <iostream>
#include <string>
#include <cmath>
#include <unordered_map>
#include <vector>
#include <queue>

#endif /* defined(__predictor__gene__) */

using namespace std;

class Gene {
    string ens_id;
    string name;
    string chrom;
    string strand;
    int start;
    int end;
    
    unordered_map<string, Transcript*> transcripts;
    
public:
    // Constructor
    Gene(string ens_id, string name, string chrom, int start, int end, string strand);
    
    void addTranscript(string trans_id, string trans_name, string biotype, int start, int end, Gene *gene);
    
    // Getters
    int getLength();
    string getName();
    string getEnsID();
    string getChromosome();
    string getStrand();
    int getStart();
    int getEnd();
    
    Transcript* getTranscript(string trans_id);
    unordered_map<string,Transcript*> getTranscripts();
    
};
