//
//  Kmer.cpp
//  predictor
//
//  Created by Pedro Furió Tarí on 17/04/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#include "Kmer.h"

Kmer::Kmer() {
    
}

void Kmer::addGene(Gene * seq) {
    
    this->seqs.insert(seq);
    
}

int Kmer::getNumberGenes() {
    
    return int(this->seqs.size());
    
}

unordered_set<Gene *> Kmer::getAllGenes() {
    return this->seqs;
}