//
//  exon.cpp
//  predictor
//
//  Created by Pedro Furió Tarí on 04/07/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#include "exon.h"

Exon::Exon(int start, int end, int number) {
  
  this->start = start;
  this->end = end;
  this->number = number;
  
}

int Exon::getStart() {
    
  return this->start;
    
}

int Exon::getEnd() {
    
  return this->end;
    
}

int Exon::getNumber() {
    
  return this->number;
    
}
