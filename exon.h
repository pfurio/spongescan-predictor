//
//  exon.h
//  predictor
//
//  Created by Pedro Furió Tarí on 04/07/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#ifndef __predictor__exon__
#define __predictor__exon__
#endif /* defined(__predictor__exon__) */

class Exon {
    int start;
    int end;
    int number;
    
public:
  
    // Constructor
    Exon(int start, int end, int number);
    
    // Getters
    int getStart();
    int getEnd();
    int getNumber();
    
};
