//
//  mirnas.cpp
//  predictor
//
//  Created by Pedro Furió Tarí on 26/04/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#include "mirnas.h"

Mirna::Mirna(string name, string tipo) {
    this->name = name;
    this->tipo = tipo;
}

void Mirna::insertTissue(string tissue) {
    this->tissues.push_back(tissue);
}

// Getters
string Mirna::getName() {
    return this->name;
}

string Mirna::getTipo() {
    return this->tipo;
}

vector<string> Mirna::getTissues() {
    return this->tissues;
}