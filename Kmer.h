//
//  Kmer.h
//  predictor
//
//  Created by Pedro Furió Tarí on 17/04/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#ifndef __predictor__Kmer__
#define __predictor__Kmer__

#include <unordered_set>
#include <iostream>
#include <cstring>

#include "gene.h"

#endif /* defined(__predictor__Kmer__) */

using namespace std;

class Kmer {
    unordered_set<Gene *> seqs;
    
public:
    Kmer();
    void addGene(Gene *);
    
    // Returns the number of sequences in which the current kmer has been found at least one time
    int getNumberGenes();     
    unordered_set<Gene *> getAllGenes();
    
};
