//
//  main2.cpp
//  predictor
//
//  Created by Pedro Furió Tarí on 04/07/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <ctime>
#include <cstdlib>
#include <numeric> // accumulate
#include <unistd.h>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/database.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/array.hpp>

//#include "gene.h"
#include "Kmer.h"
#include "mirnas.h"

using namespace std;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;

// Biotypes that will be filtered to be considered as lncRNAs
// Extracted from http://www.ensembl.org/Help/Glossary
string biotypes[10] = {
        "3prime_overlapping_ncrna",
        "ambiguous_orf",
        "antisense",
        "lincRNA",
        "ncrna_host",
        "non_coding",
        "processed_transcript",
        "retained_intron",
        "sense_intronic",
        "sense_overlapping"
};

void showHelp() {

    cout << "Usage: mypredictor [mandatory] [optional]" << endl;
    cout << "Mandatory:" << endl;
    cout << "\t-m: miRNA tab file [kmer	canonical?	mirna	tissue(s)]" << endl;
    cout << "\t-g: GTF file" << endl;
    cout << "\t-f: Fasta file with sequences" << endl;
    cout << "\t-j: Job ID" << endl;
    cout << "\t-d: Database" << endl;
    cout << "Optional:" << endl;
    cout << "\t-k: Kmer length [Default: 6]" << endl;
    cout << "\t-t: LOD Score threshold [Default: 1]" << endl;
    cout << "\t-c: Kmer Complexity Score threshold (Applied to kmers with no known miRNA [Default: 4])" << endl;
    cout << "\t-s: Maximum standard deviation allowed. [Default: 10]" << endl;
    cout << "\t-a: Minimum total binding sites detected threshold. [Default: 20]" << endl;
    cout << "\t-w: Use wobbles [Default: Yes]" << endl;
    cout << "\t-r: Fasta in negative strand in reverse complementary [Default: No]" << endl;
    cout << "\t-h: Database host:port. Default: localhost:27017" << endl;
    cout << "\t-u: Database user" << endl;
    cout << "\t-p: Database password" << endl;

    cout << endl;

}


// ------------------------------------------------------
// Auxiliary functions
// ------------------------------------------------------


// Returns all possible combinations of kmers

vector<string> kmer_creation(string sequence, int kmer_length) {

    if (sequence.length() + 1 == kmer_length) {

        vector<string> myvector{sequence + "A", sequence + "G", sequence + "C", sequence + "T"};
        return myvector;

    } else {

        vector<string> myvector;
        auto A = kmer_creation(sequence + "A", kmer_length);
        auto G = kmer_creation(sequence + "G", kmer_length);
        auto C = kmer_creation(sequence + "C", kmer_length);
        auto T = kmer_creation(sequence + "T", kmer_length);

        myvector.reserve(A.size() + G.size() + T.size() + C.size());
        myvector.insert(myvector.end(), A.begin(), A.end());
        myvector.insert(myvector.end(), G.begin(), G.end());
        myvector.insert(myvector.end(), C.begin(), C.end());
        myvector.insert(myvector.end(), T.begin(), T.end());

        return myvector;

    }

}

// Returns an score based on the number of times a dinucleotide can be found in the current kmer
// and the number of nts needed to change in order to construct a poly_nt

float compl_kmer_score(string kmer) {

    string subkmer;
    vector<string> di_nts;

    // Get the number of different di_nucleotides in kmer
    for (int i = 0; i < kmer.length() - 1; i++) {
        subkmer = kmer.substr(i, 2);
        if (find(di_nts.begin(), di_nts.end(), subkmer) == di_nts.end())
            di_nts.push_back(subkmer);
    }

    // Get the count of As, Gs, Cs, Ts in kmer
    auto As = count(kmer.begin(), kmer.end(), 'A');
    auto Gs = count(kmer.begin(), kmer.end(), 'G');
    auto Cs = count(kmer.begin(), kmer.end(), 'C');
    auto Ts = count(kmer.begin(), kmer.end(), 'T');

    auto maximo = max(Ts, max(Cs, max(As, Gs)));
    maximo = kmer.length() - maximo;

    return maximo * 0.5 + di_nts.size() * 0.5;

}

// Returns the basic information of the genes -> transcripts -> exons in a good
// structured way

void parseGTF(unordered_map<string, Gene*> &all_seqs, unordered_map<string, Transcript*> &all_transcripts, string gtf_file) {

    ifstream in(gtf_file);
    string linea;
    vector <string> split_linea;

    string chrom, gene_id, trans_id, gene_name, trans_name, biotype, aux, auxSub, strand;
    int start, end, exon_number;

    cout << "Reading GTF file..." << endl;

    if (in) {

        while (getline(in, linea)) {

            if (linea[0] != 35) { // 35 = #

                split_linea.clear();

                int pos;
                while ((pos = int(linea.find("\t"))) != -1) {
                    split_linea.push_back(linea.substr(0, pos));
                    linea = linea.substr(pos + 1, linea.length());
                }

                split_linea.push_back(linea);

                chrom = split_linea[0];
                start = atoi(split_linea[3].c_str());
                end = atoi(split_linea[4].c_str());
                aux = split_linea[8];
                strand = split_linea[6];

                bool flag_biotype = false;

                // gene_id
                pos = int(aux.find("gene_id"));
                if (pos != aux.npos) {
                    auxSub = aux.substr(pos + 1, aux.length());

                    pos = int(auxSub.find('"'));
                    auxSub = auxSub.substr(pos + 1, auxSub.length());

                    pos = int(auxSub.find('"'));
                    gene_id = auxSub.substr(0, pos);

                    // Check if there's a dot ENSG123123.1 and remove it in that case
                    pos = int(gene_id.find('.'));
                    if (pos != gene_id.npos) {
                        gene_id = gene_id.substr(0, pos);
                    }

                } else {
                    gene_id = "Unknown";
                }

                // gene_name
                pos = int(aux.find("gene_name"));
                if (pos != aux.npos) {
                    auxSub = aux.substr(pos + 1, aux.length());

                    pos = int(auxSub.find('"'));
                    auxSub = auxSub.substr(pos + 1, auxSub.length());

                    pos = int(auxSub.find('"'));
                    gene_name = auxSub.substr(0, pos);
                } else {
                    gene_name = gene_id;
                }


                // trans_id
                pos = int(aux.find("transcript_id"));
                if (pos != aux.npos) {
                    auxSub = aux.substr(pos + 1, aux.length());

                    pos = int(auxSub.find('"'));
                    auxSub = auxSub.substr(pos + 1, auxSub.length());

                    pos = int(auxSub.find('"'));
                    trans_id = auxSub.substr(0, pos);

                    // Check if there's a dot ENSG123123.1 and remove it in that case
                    pos = int(trans_id.find('.'));
                    if (pos != trans_id.npos) {
                        trans_id = trans_id.substr(0, pos);
                    }

                } else {
                    trans_id = "Unknown";
                }

                // trans_name
                pos = int(aux.find("transcript_name"));
                if (pos != aux.npos) {
                    auxSub = aux.substr(pos + 1, aux.length());

                    pos = int(auxSub.find('"'));
                    auxSub = auxSub.substr(pos + 1, auxSub.length());

                    pos = int(auxSub.find('"'));
                    trans_name = auxSub.substr(0, pos);
                } else {
                    trans_name = trans_id;
                }

                // biotype
                pos = int(aux.find("transcript_type"));
                if (pos != aux.npos) {
                    auxSub = aux.substr(pos + 1, aux.length());

                    pos = int(auxSub.find('"'));
                    auxSub = auxSub.substr(pos + 1, auxSub.length());

                    pos = int(auxSub.find('"'));
                    biotype = auxSub.substr(0, pos);

                    // Check if the biotype corresponds to a lncRNA
                    for (auto bio : biotypes) {
                        if (!biotype.compare(bio)) {
                            flag_biotype = true;
                            break;
                        }
                    }

                } else {
                    // Check with gene_biotype tag
                    pos = int(aux.find("transcript_biotype"));
                    if (pos != aux.npos) {
                        auxSub = aux.substr(pos + 1, aux.length());

                        pos = int(auxSub.find('"'));
                        auxSub = auxSub.substr(pos + 1, auxSub.length());

                        pos = int(auxSub.find('"'));
                        biotype = auxSub.substr(0, pos);

                        // Check if the biotype corresponds to a lncRNA
                        for (auto bio : biotypes) {
                            if (!biotype.compare(bio)) {
                                flag_biotype = true;
                                break;
                            }
                        }

                    } else {
                        biotype = "Unknown";
                        flag_biotype = true;
                    }

                }


                if (flag_biotype) {
                    Gene *gene = NULL;

                    if (!split_linea[2].compare("exon")) {


                        if (all_seqs.count(gene_id) == 0) {
                            //cout << "Warning: The gene " << gene_id << " has not been previously defined" << endl;

                            gene = new Gene(gene_id, gene_name, chrom, start, end, strand);
                            all_seqs.insert(make_pair(gene_id, gene));

                        }

                        gene = all_seqs[gene_id];

                        if (gene->getTranscript(trans_id) == NULL) {
                            //cout << "Warning: The transcript " << trans_id << " has not been previously defined" << endl;

                            gene->addTranscript(trans_id, trans_name, biotype, start, end, gene);
                            all_transcripts.insert(make_pair(trans_id, gene->getTranscript(trans_id)));
                        }

                        Transcript *transcript = gene->getTranscript(trans_id);

                        // trans_name
                        pos = int(aux.find("exon_number"));
                        if (pos != aux.npos) {
                            auxSub = aux.substr(pos + 1, aux.length());

                            pos = int(auxSub.find(" "));
                            auxSub = auxSub.substr(pos + 1, auxSub.length());

                            pos = int(auxSub.find(";"));
                            if (pos >= 3) // This means that the gtf is as follows: exon_number "3"; instead of exon_number 3;
                                exon_number = atoi(auxSub.substr(1, pos - 1).c_str());
                            else
                                exon_number = atoi(auxSub.substr(0, pos).c_str());
                        } else {
                            exon_number = -1;
                        }

                        transcript->addExon(start, end, exon_number);


                    } else if (!split_linea[2].compare("transcript")) {

                        if (all_seqs.count(gene_id) == 0) {
                            //cout << "Warning: The gene " << gene_id << " has not been previously defined" << endl;

                            gene = new Gene(gene_id, gene_name, chrom, start, end, strand);
                            all_seqs.insert(make_pair(gene_id, gene));

                        }

                        Gene *gene = all_seqs[gene_id];
                        gene->addTranscript(trans_id, trans_name, biotype, start, end, gene);
                        all_transcripts.insert(make_pair(trans_id, gene->getTranscript(trans_id)));

                    }

                }

            }

        }

    }

    in.close();

}

void readmiRNAsInformation(string mirnas_file, unordered_map<string, vector < Mirna*>> &all_mirnas) {
    /*
     CAGTGT  canonical       hsa-miR-6757-3p Renal cell cancer stages vs normal
     CCGTAT  canonical       mmu-miR-7040-5p Rheumatoid Arthritis Synovial Fibroblasts (MM),MSCs of different aged mice (MM)
     TAGAGT  canonical       hsa-miR-5684    Human cell line after Dicer silencing
     */

    cout << "Reading miRNAs file..." << endl;

    // Read the fasta file
    ifstream in(mirnas_file);
    vector <string> split_linea;
    vector <string> tissue;
    string linea;
    string tissues;

    // If the file actually exists ...
    if (in) {
        while (getline(in, linea)) {

            split_linea.clear();

            int pos;
            while ((pos = int(linea.find("\t"))) != -1) {
                split_linea.push_back(linea.substr(0, pos));
                linea = linea.substr(pos + 1, linea.length());
            }

            Mirna *seq = new Mirna(split_linea[2], split_linea[1]);

            /*
             * We won't be looking at tissues in the miRNA file
            if (!linea.empty()) {
                tissues = linea.substr(pos + 1, linea.length());

                while ((pos = int(linea.find(","))) != -1) {
                    seq->insertTissue(linea.substr(0, pos));
                    linea = linea.substr(pos + 1, linea.length());
                }
                seq->insertTissue(linea.substr(0, linea.length()));
            }
             */

            // If it's the first time we see the key, we create the instance with an empty vector
            if (all_mirnas.count(split_linea[0]) == 0) {
                vector<Mirna *> m;
                all_mirnas.insert(make_pair(split_linea[0], m));
            }

            all_mirnas[split_linea[0]].push_back(seq);

        }
    }
    in.close();
    cout << "miRNAs file read" << endl;
}


// ------------------------------------------------------
// Main functions
// ------------------------------------------------------

void analyze_file(string fasta, int kmer_length, unordered_map<string, Transcript*> &all_transcripts,
                  unordered_map<string, Kmer*> &all_kmers, bool wobbles, unordered_map<string, vector < Mirna*>> &all_mirnas, bool reverse) {

    // Variable definition + initialization
    string linea;
    string header;
    string sequence;

    // I create a multimap in the following way:
    // Key: each kmer
    // Value: Each possible kmer (above the complexity score) after changing the wobbles that have a mirna known.
    //        If there's none, the value will be the same as the key
    unordered_multimap<string, string> mirnas_wobbles;

    if (wobbles) {

        for (auto it_hash : all_kmers) {
            string kmer = it_hash.first;

            // If current kmer is a known mirna
            if (all_mirnas.count(kmer) > 0)
                mirnas_wobbles.insert(make_pair(kmer, kmer));

            for (int j = 0; j < kmer_length; j++) {
                if (kmer[j] == 'T' || kmer[j] == 'G') {
                    string aux_kmer = kmer;
                    aux_kmer[j] = aux_kmer[j] == 'T' ? 'C' : 'A';

                    // If the kmer is considered and there's no known mirna
                    if (all_kmers.count(aux_kmer) > 0 && all_mirnas.count(aux_kmer) == 0) {
                        mirnas_wobbles.insert(make_pair(kmer, aux_kmer));
                    }
                }
            }

            // If I haven't found any known mirna after doing the wobble conversion, I add it with itself
            if (mirnas_wobbles.count(kmer) == 0)
                mirnas_wobbles.insert(make_pair(kmer, kmer));
        }

    } else {

        for (auto it_hash : all_kmers) {
            string kmer = it_hash.first;
            mirnas_wobbles.insert(make_pair(kmer, kmer));
        }

    }

    clock_t start = clock();

    cout << "Analyzing fasta file..." << endl;

    // Read the fasta file
    ifstream in(fasta);
    int cont = 0;
    // If the file actually exists ...
    if (in) {
        while (getline(in, linea)) {

            // We're reading the header
            if (linea.at(0) == '>') {

                // We have a sequence to analyze
                // If the sequence is not empty
                // & sequence is longer than the kmer length
                // & the sequence has been previously annotated
                if (!sequence.empty() && sequence.length() > kmer_length && all_transcripts.count(header) > 0) {
                    // transform(sequence.begin(),sequence.end(),sequence.begin(),::toupper);
                    // First, check if the current transcript_id has been found in the GTF file...
                    if (all_transcripts.find(header) != all_transcripts.end()) {
                        Transcript *node = all_transcripts[header];

                        // We have to make the complementary reverse
                        if (reverse && node->getGene()->getStrand() == "-") {
                            string reverseSequence;
                            //reverseSequence.reserve(sequence.length());

                            int longitud = int(sequence.length());

                            for (int i = 0; i < longitud; i++) {
                                if (sequence[longitud - i - 1] == 65) // A
                                    reverseSequence += 'T';
                                else if (sequence[longitud - i - 1] == 84) // T
                                    reverseSequence += 'A';
                                else if (sequence[longitud - i - 1] == 67) // C
                                    reverseSequence += 'G';
                                else if (sequence[longitud - i - 1] == 71) // G
                                    reverseSequence += 'C';
                                else
                                    reverseSequence += sequence[longitud - i - 1];

                            }

                            sequence = reverseSequence;
                        }

                        // Now we are going to get all the kmers one by one
                        for (int i = 0; i < sequence.length() - kmer_length + 1; i++) {

                            string subkmer = sequence.substr(i, kmer_length);

                            // Take only upper letters (Non-repetitive)
                            bool weirdElement = false;
                            for (auto it = 0; it < subkmer.size(); it++) {
                                if (subkmer[it] != 65 && subkmer[it] != 67 && subkmer[it] != 71 && subkmer[it] != 84)
                                    weirdElement = true;
                            }

                            if (!weirdElement && all_kmers.count(subkmer) > 0) { // If there's no N, actg ...

                                auto its = mirnas_wobbles.equal_range(subkmer);
                                for (auto it = its.first; it != its.second; ++it) {

                                    // If its the first appearance of kmer - seq.id
                                    if (!node->addPosition(it->second, i)) // We add the position in the sequence class
                                        all_kmers[it->second]->addGene(node->getGene()); // Insert a pointer to the sequence in the kmer
                                    // !subkmer.compare(\"GCACAG\") && !header.compare(\"ENST00000427317.1\")
                                }

                            }
                        }

                        // We calculate and see which of the kmers doesn't have a proper separation between them
                        node->setInnerDistance();

                        cont += 1;
                        if (cont % 1000 == 0) {
                            cout << "It took " << (clock() - start) / (double) CLOCKS_PER_SEC << " seconds." << endl;
                            cout << cont << endl;
                            start = clock();
                        }
                    }
                }

                // Split the header in order to get the first string
                // > ENSG00000 blabla blabla
                // header will contain ENSG00000

                unsigned long secondspace = linea.substr(1, linea.length()).find_first_of(" \t");
                if (secondspace == linea.npos)
                    secondspace = linea.length();

                header = linea.substr(1, secondspace);

                // Check if there's a dot ENSG123123.1 and remove it in that case
                int pos = int(header.find('.'));
                if (pos != header.npos) {
                    header = header.substr(0, pos);
                }

                sequence = "";
            } else {
                sequence += linea;
            }

        }

    } else {
        cout << "Couldn't read the file" << endl;
    }

    // And the storing of the last sequence
    // First, check if the current transcript_id has been found in the GTF file...
    if (all_transcripts.find(header) != all_transcripts.end()) {
        Transcript *node = all_transcripts[header];

        // We have to make the complementary reverse
        if (reverse && node->getGene()->getStrand() == "-") {
            string reverseSequence;
            //reverseSequence.reserve(sequence.length());

            int longitud = int(sequence.length());

            for (int i = 0; i < longitud; i++) {
                if (sequence[longitud - i - 1] == 65) // A
                    reverseSequence += 'T';
                else if (sequence[longitud - i - 1] == 84) // T
                    reverseSequence += 'A';
                else if (sequence[longitud - i - 1] == 67) // C
                    reverseSequence += 'G';
                else if (sequence[longitud - i - 1] == 71) // G
                    reverseSequence += 'C';
                else
                    reverseSequence += sequence[longitud - i - 1];

            }

            sequence = reverseSequence;
        }

        // Now we are going to get all the kmers one by one
        for (int i = 0; i < sequence.length() - kmer_length + 1; i++) {

            string subkmer = sequence.substr(i, kmer_length);

            // Take only upper letters (Non-repetitive)
            bool weirdElement = false;
            for (auto it = 0; it < subkmer.size(); it++) {
                if (subkmer[it] != 65 && subkmer[it] != 67 && subkmer[it] != 71 && subkmer[it] != 84)
                    weirdElement = true;
            }

            if (!weirdElement && all_kmers.count(subkmer) > 0) { // If there's no N, actg ...

                auto its = mirnas_wobbles.equal_range(subkmer);
                for (auto it = its.first; it != its.second; ++it) {

                    // If its the first appearance of kmer - seq.id
                    if (!node->addPosition(it->second, i)) // We add the position in the sequence class
                        all_kmers[it->second]->addGene(node->getGene()); // Insert a pointer to the sequence in the kmer
                    // !subkmer.compare(\"GCACAG\") && !header.compare(\"ENST00000427317.1\")
                }

            }
        }

        // We calculate and see which of the kmers doesn't have a proper separation between them
        node->setInnerDistance();
    }

    cout << "Fasta file analyzed." << endl;

}

vector<unordered_map<string, float>> getLODs(unordered_map<string, Kmer *> all_kmers) {

    cout << "Calculating LOD Scores..." << endl;

    vector<unordered_map<string, float>> lods_vector;
    string kmer;

    for (int w_size = 50; w_size <= 1000; w_size += 50) {

        unordered_map<string, float> result;

        for (auto kmer_i : all_kmers) {

            kmer = kmer_i.first;

            int total_counts = 0;
            int total_size = 0;
            auto genes = all_kmers[kmer]->getAllGenes();

            // Get the maximum or total number of occurrences in which we can find the current kmer in all the sequences
            // in which we already found it
            for (auto gen_i : genes) {

                unordered_map<string, Transcript*> transcripts = gen_i->getTranscripts();

                int max_counts_trans = 0;

                for (auto transcript_i : transcripts) {

                    Transcript *transcript = transcript_i.second;

                    if (transcript->getInnerDistance(kmer)) { // Only will be taken into account kmers with a proper separation between them
                        int aux = transcript->getMaxOcurrences(w_size, kmer);

                        if (aux > max_counts_trans)
                            max_counts_trans = aux;

                    }

                }

                if (max_counts_trans > 0) {

                    total_counts += max_counts_trans;
                    total_size += w_size;

                }

            }

            result.insert(make_pair(kmer, (float) total_counts / total_size));

        }

        lods_vector.push_back(result);
        cout << "LODs for window size of " << w_size << " finished" << endl;

    }

    return lods_vector;

}

void generate_output(unordered_map<string, Gene*> all_seqs, unordered_map<string, Kmer*> all_kmers,
                     unordered_map<string, vector < Mirna*>> all_mirnas, unordered_map<string, float> kmer_scores,
                     vector<unordered_map<string, float>> lods_vector, float lods_threshold, string job_id, int kmer_size,
                     int sdev, int min_sites, string host, string database, string user, string pass) {

    mongocxx::instance instance{};
    mongocxx::client myClient;
    if (!user.empty()) {
        //    uri{"mongodb://user1:pwd1@host1/?authSource=db1&authMechanism=SCRAM-SHA-1"}};
        myClient = mongocxx::client{mongocxx::uri{"mongodb://" + user + ":" + pass + "@" + host}};
    } else {
        myClient = mongocxx::client{mongocxx::uri{host}};
    }
    mongocxx::database db = myClient[database];

    cout << "Generating output..." << endl;

    // Start the clock
    clock_t start = clock();

    // Output file with LOD score >= 3
    string tofile;


    //BSONArrayBuilder bab_bindings;
    for (auto i : all_kmers) {

        string kmer_i = i.first;

        auto seqs_kmer = all_kmers[kmer_i]->getAllGenes();

        for (auto gen_i : seqs_kmer) {

            unordered_map<string, Transcript*> transcripts = gen_i->getTranscripts();

            for (auto transcript_i : transcripts) {

                Transcript *trans_i = transcript_i.second;

                // If there's enough separation between all the binding sites for the current kmer
                if (trans_i->getInnerDistance(kmer_i) && trans_i->getLength() >= 50) {

                    // Get the maximum number of binding sites within each different window size
                    vector <int> maximum_vect;
                    for (int w_size = 50; w_size <= 1000; w_size = w_size + 50) {
                        if (trans_i->getLength() > w_size)
                            maximum_vect.push_back((float) w_size / trans_i->getMaxOcurrences(w_size, kmer_i));
                    }

                    if (maximum_vect.size() == 0) {
                        continue;
                    }

                    //int maximum = seq_i->getMaxOcurrences(w_size, kmer_i);
                    int total = trans_i->getTotalOccurrences(kmer_i);
                    // float lods = log(((float)maximum/w_size)/kmer_LODs[kmer_i]);
                    float media, desv, suma = 0, accum = 0;
                    for (auto valor : maximum_vect)
                        suma += valor;
                    media = suma / maximum_vect.size();

                    for (auto valor : maximum_vect)
                        accum += (valor - media) * (valor - media);
                    desv = sqrt(accum / (maximum_vect.size() - 1));
                    float minimo = 1000;
                    int posicion_w = 0;
                    for (int pos_w = 0; pos_w < maximum_vect.size(); pos_w++) {
                        float error = abs(media - maximum_vect[pos_w]);
                        if (error < minimo) {
                            minimo = error;
                            posicion_w = pos_w;
                        }

                    }

                    int w_size = 50 * (posicion_w + 1);
                    int maximum = w_size / maximum_vect[posicion_w];
                    float lods = log((float(maximum) / w_size) / lods_vector[posicion_w][kmer_i]);

                    if (desv > 0 && desv < sdev && total >= min_sites && lods >= lods_threshold) {

                        auto builder = bsoncxx::builder::stream::document{};
                        bsoncxx::document::value myjson = builder
                                << "kmer" << kmer_i
                                << "trans_id" << trans_i->getTransID()
                                << "gene_id" << gen_i->getEnsID()
                                << "gene_name" << gen_i->getName()
                                << "chrom" << gen_i->getChromosome()
                                << "start" << gen_i->getStart()
                                << "end" << gen_i->getEnd()
                                << "strand" << gen_i->getStrand()
                                << "biotype" << trans_i->getBiotype()
                                << "trans_name" << trans_i->getTransName()
                                << "trans_start" << trans_i->getStart()
                                << "trans_end" << trans_i->getEnd()
                                << "lod_score" << lods
                                << "compl_score" << kmer_scores[kmer_i]
                                << "occ" << maximum
                                << "w_size" << w_size
                                << "total" << total
                                << "sd" << desv
                                << "median_distance" << trans_i->getMedianDistance(kmer_i)
                                << bsoncxx::builder::stream::finalize;


                        // Add exons
                        bsoncxx::builder::basic::array arrayBuilder;
                        auto myexons = trans_i->getExons();
                        for (auto myexon : myexons) {
                            bsoncxx::document::value exonJson = builder
                                    << "exon_number" << myexon->getNumber()
                                    << "start" << myexon->getStart()
                                    << "end" << myexon->getEnd()
                                    << bsoncxx::builder::stream::finalize;
                        myjson.append("exons", bab_exons.arr());

                        // Add binding sites
                        BSONArrayBuilder bab;
                        auto positions = trans_i->getPositions(kmer_i);
                        for (auto p : positions) {
                            bab.append(p);
                        }
                        myjson.appendArray("positions", bab.arr());

                        // miRNAs
                        if (all_mirnas.count(kmer_i) > 0) {
                            BSONArrayBuilder bab;

                            auto mirnas = all_mirnas[kmer_i];
                            for (auto mirna : mirnas) {
                                bab.append(mirna->getName());
                            }
                            myjson.appendArray("mirnas", bab.arr());
                        }

                        myjson.append("job_id", job_id);
                        myjson.append("kmer_size", kmer_size);
                        myClient.insert(database + ".sponges", myjson.obj());
                    }

                }

            }
        }

    }
    /*
    bob myjson;
    myjson.append("job_id", job_id);
    myjson.append("kmer_size", kmer_size);
    myjson.append("bindings", bab_bindings.arr());

    myconnection.insert(database + ".sponges", myjson.obj());
     */
    cout << "It took " << (clock() - start) / (double) CLOCKS_PER_SEC <<
         " to create the output " << endl;

}

int main(int argc, char * argv[]) {

    // Variable definition
    int kmer_length = 6;
    float complexity_threshold = 4;
    float lod_threshold = 1;
    string mirna_Info, gtf_file, fasta_file, job_id;
    bool wobbles = true;
    bool reverse = false;
    string host = "localhost";
    string database, user, password;
    int sdev = 10;
    int min_sites = 20;

    // Reading arguments
    int c;
    while ((c = getopt(argc, argv, "m:g:f:k:t:c:wrj:h:d:u:p:s:a:")) != -1) {
        switch (c) {
            case 'm':
                mirna_Info = optarg;
                break;
            case 'g':
                gtf_file = optarg;
                break;
            case 'f':
                fasta_file = optarg;
                break;
            case 'k':
                kmer_length = atoi(optarg);
                break;
            case 't':
                lod_threshold = atof(optarg);
                break;
            case 'c':
                complexity_threshold = atof(optarg);
                break;
            case 'w':
                wobbles = false;
                break;
            case 'r':
                reverse = true;
                break;
            case 'j':
                job_id = optarg;
                break;
            case 'h':
                host = optarg;
                break;
            case 'd':
                database = optarg;
                break;
            case 'u':
                user = optarg;
                break;
            case 'p':
                password = optarg;
                break;
            case 's':
                sdev = atoi(optarg);
                break;
            case 'a':
                min_sites = atoi(optarg);
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (mirna_Info.empty() || gtf_file.empty() || fasta_file.empty() || job_id.empty() || database.empty()) {
        showHelp();
        exit(1);
    }


    // 1. Read the miRNAs information file
    unordered_map<string, vector < Mirna*>> all_mirnas;
    readmiRNAsInformation(mirna_Info, all_mirnas);

    // 2. Create all possible kmers and check their complexity
    unordered_map<string, Kmer *> all_kmers;
    unordered_map<string, float> filtered_kmers;

    auto kmers = kmer_creation("", kmer_length);

    for (auto kmer : kmers) {
        float score = compl_kmer_score(kmer);
        // If the current kmer is a miRNA or has a complexity score over the threshold
        if (all_mirnas.count(kmer) > 0 || score >= complexity_threshold) {
            all_kmers.insert(make_pair(kmer, new Kmer()));
            filtered_kmers.insert(make_pair(kmer, score));
        }
    }

    // 3. Read the GTF file
    unordered_map<string, Gene*> all_genes;
    unordered_map<string, Transcript*> all_transcripts;
    parseGTF(all_genes, all_transcripts, gtf_file);

    // 4. Analyze the fasta file
    analyze_file(fasta_file, kmer_length, all_transcripts, all_kmers, wobbles,
                 all_mirnas, reverse);

    // 5. Get LOD scores
    vector<unordered_map<string, float>> lods_vector = getLODs(all_kmers);

    // 6. Generate results
    generate_output(all_genes, all_kmers, all_mirnas, filtered_kmers,
                    lods_vector, lod_threshold, job_id, kmer_length, sdev, min_sites,
                    host, database, user, password);

    return 0;
}

