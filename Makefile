CC=g++
CFLAGS=-Wall -O3 -pedantic
MONGOLIB=-I/opt/mongo-cxx-driver/include/mongocxx/v_noabi -I/opt/mongo-cxx-driver/include/bsoncxx/v_noabi -L/opt/mongo-cxx-driver/lib
BOOSTFLAGS=-pthread -lmongoclient -lboost_thread -lboost_system -lboost_regex -lboost_filesystem -lboost_program_options

all: predictor

predictor: Kmer.o exon.o transcript.o gene.o mirnas.o
	    $(CC) -std=c++11 $(MONGOLIB) -o predictor main.cpp Kmer.o mirnas.o exon.o transcript.o gene.o -static -O3 $(BOOSTFLAGS)

mirnas.o: mirnas.cpp
	    $(CC) -std=c++11 -c mirnas.cpp $(CFLAGS)

Kmer.o: Kmer.cpp
	    $(CC) -std=c++11 -c Kmer.cpp $(CFLAGS)

exon.o: exon.cpp
	    $(CC) -std=c++11  -c exon.cpp $(CFLAGS)

transcript.o: transcript.cpp
	    $(CC) -std=c++11 -c transcript.cpp $(CFLAGS)

gene.o: gene.cpp
	    $(CC) -std=c++11 -c gene.cpp $(CFLAGS)

clean:
	    rm -rf *o predictor
