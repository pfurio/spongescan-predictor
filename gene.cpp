//
//  gene.cpp
//  predictor
//
//  Created by Pedro Furió Tarí on 04/07/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#include "gene.h"

// Public methods

Gene::Gene(string ens_id, string name, string chrom, int start, int end, string strand) {
    this->ens_id = ens_id;
    this->name = name;
    this->chrom = chrom;
    this->start = start;
    this->end = end;
    this->strand = strand;
}

void Gene::addTranscript (string trans_id, string trans_name, string biotype, int start, int end, Gene *gene) {
  this->transcripts.insert(make_pair(trans_id, new Transcript(trans_id, trans_name, biotype, start, end, gene)));
}

int Gene::getLength() {
    return this->end - this->start + 1;
}

string Gene::getName() {
    return this->name;
}

string Gene::getEnsID() {
    return this->ens_id;
}

string Gene::getChromosome() {
    return this->chrom;
}

string Gene::getStrand() {
    return this->strand;
}

int Gene::getStart() {
    return this->start;
}

int Gene::getEnd() {
    return this->end;
}

Transcript* Gene::getTranscript(string trans_id) {
  if (this->transcripts.count(trans_id) > 0)
    return this->transcripts[trans_id];
  
  return NULL;
}

unordered_map<string,Transcript*> Gene::getTranscripts() {
 return this->transcripts; 
}
