//
//  mirnas.h
//  predictor
//
//  Created by Pedro Furió Tarí on 26/04/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#ifndef predictor_mirnas_h
#define predictor_mirnas_h

#include <string>
#include <vector>

#endif

using namespace std;

class Mirna {
    string name; 
    string tipo; // Canonical ...
    vector<string> tissues;
    
public:
    // Constructor + setters
    Mirna(string name, string tipo);
    void insertTissue(string tissue);
    
    // Getters
    string getName();
    string getTipo();
    vector<string> getTissues();
};
