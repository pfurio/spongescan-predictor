//
//  transcript.h
//  predictor
//
//  Created by Pedro Furió Tarí on 04/07/14.
//  Copyright (c) 2014 pfurio. All rights reserved.
//

#include "transcript.h"

// Public methods

Transcript::Transcript(string trans_id, string trans_name, string biotype, int start, int end, Gene *gene) {
  
    this->trans_id = trans_id;
    this->trans_name = trans_name;
    this->biotype = biotype;
    this->start = start;
    this->end = end;
    this->gene = gene;
    
}

void Transcript::addExon(int start, int end, int number) {
  
    // Add Exon
    this->exons.push_back(new Exon(start, end, number));
  
}

int Transcript::addPosition(string kmer, int position) {

    int value = this->positions.count(kmer);
    if (value == 0) {
        vector<int> x;
        this->positions.insert(make_pair(kmer,x));
    }
    
    this->positions[kmer].push_back(position);
    
    return value;
}

void Transcript::setInnerDistance() {
    
    for (auto i : this->positions) {
        string kmer = i.first;
        auto myvector = i.second;
        
        int old_pos = -1000;
        int dist_allowed = int(kmer.size());
        int flag = 1;
        for (int pos : myvector) {
            if (pos - old_pos < dist_allowed) {
                flag = 0;
                break;
            }
            old_pos = pos;
        }
        
        this->inner_distance.insert(make_pair(kmer,flag));
        
    }
    
}


// All getters

int Transcript::getLength() {
  
  int tamanyo = 0;
  
  for (auto exon: this->exons) {
      tamanyo += exon->getEnd() - exon->getStart() + 1;
  }
  
  return tamanyo;
  
}

string Transcript::getTransID() {
 
  return this->trans_id;
  
}

string Transcript::getTransName() {
 
  return this->trans_name;
  
}

string Transcript::getBiotype () {
    return this->biotype;
}

int Transcript::getStart() {
 
  return this->start;
  
}

int Transcript::getEnd() {
 
  return this->end;
  
}

vector<Exon*> Transcript::getExons() {
 
  return this->exons;
  
}

int Transcript::getInnerDistance(string kmer) {
    return this->inner_distance[kmer];
}

int Transcript::getTotalOccurrences(string kmer) {
    return int(this->positions[kmer].size());
}

int Transcript::getMaxOcurrences(int sliding_window, string kmer) {
    queue<int> myqueue;
    int maximo = 0;
    auto longi_kmer = int(kmer.length());
    
    for (auto pos : this->positions[kmer]) {
  
        // While the current position - front from the queue are not lower than the sliding window, remove front ...
        while (!myqueue.empty() && pos - longi_kmer > int(myqueue.front()) + sliding_window)
            myqueue.pop();
        
        // Add the current position to the queue
        myqueue.push(pos);
        
        // Check if it's maximum
        maximo = max(maximo, int(myqueue.size()));
    }
    
    return maximo;
}

int Transcript::getMedianDistance(string kmer) {

    vector<int> distances;

    for (int i = 1; i < this->positions[kmer].size(); i++) {
        distances.push_back(this->positions[kmer][i] - this->positions[kmer][i-1] + 1);
    }

    std::sort (distances.begin(), distances.end());
    return distances[distances.size()/2];

}

vector<string> Transcript::getAllKmers() {
    vector<string> keys;
    
    for (auto pair:this->positions)
        keys.push_back(pair.first);
    
    return keys;
}

vector<int> Transcript::getPositions(string kmer) {
    
    return this->positions[kmer];
    
}

Gene * Transcript::getGene() {
    return this->gene;
}
